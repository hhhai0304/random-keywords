package com.hohoanghai.tikihometest.common.base

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T : BaseModel>(private val context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bind(item: T, position: Int)

    fun getString(id: Int): String {
        return context.resources.getString(id)
    }
}