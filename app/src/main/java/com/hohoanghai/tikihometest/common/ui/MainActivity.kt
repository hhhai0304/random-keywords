package com.hohoanghai.tikihometest.common.ui

import com.hohoanghai.tikihometest.R
import com.hohoanghai.tikihometest.common.base.BaseActivity
import com.hohoanghai.tikihometest.feature.home.HomeFragment

class MainActivity : BaseActivity(R.layout.activity_main) {

    override val fragmentContainer: Int
        get() = R.id.fragmentContainer

    override fun setupEvent() {
        addFragment(HomeFragment.newInstance())
    }
}
