package com.hohoanghai.tikihometest.common.network

import io.reactivex.Single
import retrofit2.http.GET

interface ApiInterface {
    @GET("keywords.json")
    fun getKeywords(): Single<ArrayList<String>>
}