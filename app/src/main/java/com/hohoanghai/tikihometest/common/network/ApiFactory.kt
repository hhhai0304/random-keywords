package com.hohoanghai.tikihometest.common.network

import com.hohoanghai.tikihometest.BuildConfig
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiFactory {

    private const val TIME_OUT_READ = 30
    private const val TIME_OUT_WRITE = 90

    private val dispatcher = Dispatcher()

    private val authInterceptor = Interceptor {
        val newUrl = it.request().url().newBuilder().build()
        val newRequest = it.request().newBuilder().url(newUrl).build()
        it.proceed(newRequest)
    }

    fun retrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }

        val client = OkHttpClient().newBuilder()
                .addInterceptor(authInterceptor)
                .addInterceptor(interceptor)
                .connectTimeout(TIME_OUT_READ.toLong(), TimeUnit.SECONDS)
                .readTimeout(TIME_OUT_READ.toLong(), TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT_WRITE.toLong(), TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .dispatcher(dispatcher)
                .build()

        return Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}