package com.hohoanghai.tikihometest.common.util

object Utils {

    fun calculateBreakWord(input: String): String {
        val array = input.trim().split(" ")
        return when (array.size) {
            0 -> ""
            1 -> array[0]
            2 -> array[0] + "\n" + array[1]
            else -> {
                val totalLength = input.length - (array.size - 1)
                val differenceIndex = ArrayList<Int>()
                var leftSideSum = 0
                array.forEach {
                    leftSideSum += it.length
                    val rightSideSum = totalLength - leftSideSum
                    differenceIndex.add(Math.abs(leftSideSum - rightSideSum))
                }
                val minDifferenceIndex = differenceIndex.indexOf(differenceIndex.min())

                val builder = StringBuilder()
                for (i in array.indices) {
                    if (i != 0 && i != (minDifferenceIndex + 1)) {
                        builder.append(" ")
                    }
                    builder.append(array[i])
                    if (i == minDifferenceIndex) {
                        builder.append("\n")
                    }
                }
                builder.toString()
            }
        }
    }
}