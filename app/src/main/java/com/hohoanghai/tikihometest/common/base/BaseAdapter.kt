package com.hohoanghai.tikihometest.common.base

import android.content.Context
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T : BaseModel, VH : BaseViewHolder<T>>(val context: Context, val data: ArrayList<T>) : RecyclerView.Adapter<VH>() {

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(data[position], position)
    }

    fun add(item: T) {
        data.add(item)
        val position = data.indexOf(item)
        notifyItemInserted(position)
    }

    fun addAll(items: List<T>) {
        val startPosition = data.size
        data.addAll(items)
        notifyItemRangeInserted(startPosition, items.size)
    }

    fun replaceAll(items: List<T>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }

    fun remove(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    fun clear() {
        data.clear()
        notifyDataSetChanged()
    }
}