package com.hohoanghai.tikihometest.common.model

import com.hohoanghai.tikihometest.common.base.BaseModel

data class Keyword(var keyword: String, var color: Int) : BaseModel()