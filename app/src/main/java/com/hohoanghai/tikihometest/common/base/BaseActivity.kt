package com.hohoanghai.tikihometest.common.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

abstract class BaseActivity(private val layoutId: Int) : AppCompatActivity() {

    protected abstract val fragmentContainer: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        setupEvent()
    }

    protected abstract fun setupEvent()

    protected fun addFragment(fragment: BaseFragment, tag: String? = fragment.javaClass.simpleName) {
        supportFragmentManager.beginTransaction().add(fragmentContainer, fragment, tag).commit()
    }

    protected fun replaceFragment(fragment: BaseFragment, tag: String? = fragment.javaClass.simpleName) {
        supportFragmentManager.beginTransaction().replace(fragmentContainer, fragment, tag).commit()
    }

    fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    fun hideLoading() {
        progressBar?.visibility = View.GONE
    }
}