package com.hohoanghai.tikihometest.common.base

import android.content.Context
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<V : BaseView>(var context: Context?, var view: V) {

    private var compositeDisposable = CompositeDisposable()
    var isRefresh = false

    fun onDetach() {
        compositeDisposable.dispose()
    }

    fun addDisposable(d: Disposable?) {
        d?.let {
            compositeDisposable.add(it)
        }
    }

    fun removeDisposable(d: Disposable?) {
        d?.let {
            compositeDisposable.remove(d)
            hideLoading()
        }
    }

    fun clearDisposable() {
        compositeDisposable.clear()
        hideLoading()
    }

    fun showLoading() {
        view.showLoading()
    }

    fun hideLoading() {
        view.hideLoading()
        isRefresh = false
    }
}