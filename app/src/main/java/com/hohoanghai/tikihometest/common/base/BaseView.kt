package com.hohoanghai.tikihometest.common.base

interface BaseView {
    fun showLoading()
    fun hideLoading()
}