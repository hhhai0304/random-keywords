package com.hohoanghai.tikihometest.feature.home

import com.hohoanghai.tikihometest.common.base.BaseView
import com.hohoanghai.tikihometest.common.model.Keyword

interface HomeView : BaseView {
    fun onGetKeywordsCompleted(keywords: ArrayList<Keyword>, e: Throwable? = null)
}