package com.hohoanghai.tikihometest.feature.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.hohoanghai.tikihometest.R
import com.hohoanghai.tikihometest.common.base.BaseAdapter
import com.hohoanghai.tikihometest.common.base.BaseViewHolder
import com.hohoanghai.tikihometest.common.model.Keyword

class HomeAdapter(context: Context, data: ArrayList<Keyword>) : BaseAdapter<Keyword, HomeAdapter.ViewHolder>(context, data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(context, LayoutInflater.from(parent.context).inflate(R.layout.item_keyword, parent, false))
    }

    class ViewHolder(context: Context, itemView: View) : BaseViewHolder<Keyword>(context, itemView) {
        override fun bind(item: Keyword, position: Int) {
            val cvKeyword = itemView.findViewById<CardView>(R.id.cvKeyword)
            val tvKeyword = itemView.findViewById<TextView>(R.id.tvKeyword)
            cvKeyword.setCardBackgroundColor(item.color)
            tvKeyword.text = item.keyword
        }
    }
}