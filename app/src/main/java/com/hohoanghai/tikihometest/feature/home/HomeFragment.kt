package com.hohoanghai.tikihometest.feature.home

import android.view.View
import com.hohoanghai.tikihometest.R
import com.hohoanghai.tikihometest.common.base.BaseFragment
import com.hohoanghai.tikihometest.common.model.Keyword
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment(R.layout.fragment_home), HomeView {
    private lateinit var presenter: HomePresenter
    private val data = ArrayList<Keyword>()
    private lateinit var adapter: HomeAdapter

    companion object {
        fun newInstance(): BaseFragment {
            return HomeFragment()
        }
    }

    override fun setupMvp() {
        presenter = HomePresenter(context, this)
    }

    override fun setupEvent(view: View) {
        setTitle(resources.getString(R.string.home_fragment_title))

        tvNotice.setOnClickListener {
            it.visibility = View.GONE
            presenter.getKeyword()
        }

        adapter = HomeAdapter(context!!, data)
        rvKeyword.adapter = adapter

        presenter.getKeyword()
    }

    override fun onGetKeywordsCompleted(keywords: ArrayList<Keyword>, e: Throwable?) {
        e?.let {
            tvNotice.visibility = View.VISIBLE
            return
        }

        tvNotice.visibility = View.GONE
        adapter.replaceAll(keywords)
    }
}