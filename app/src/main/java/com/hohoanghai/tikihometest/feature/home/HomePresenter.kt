package com.hohoanghai.tikihometest.feature.home

import android.content.Context
import android.graphics.Color
import android.util.Log
import com.hohoanghai.tikihometest.common.base.BasePresenter
import com.hohoanghai.tikihometest.common.model.Keyword
import com.hohoanghai.tikihometest.common.network.ApiFactory
import com.hohoanghai.tikihometest.common.network.ApiInterface
import com.hohoanghai.tikihometest.common.util.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlin.random.Random

class HomePresenter(context: Context?, view: HomeView) : BasePresenter<HomeView>(context, view) {
    private val api = ApiFactory.retrofit().create(ApiInterface::class.java)

    fun getKeyword() {
        showLoading()
        addDisposable(api.getKeywords().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val result = ArrayList<Keyword>()

                    val darkColorValue = 150
                    val random = Random(darkColorValue)

                    it.forEach { keyword ->
                        val red = random.nextInt(darkColorValue)
                        val green = random.nextInt(darkColorValue)
                        val blue = random.nextInt(darkColorValue)

                        val color = Color.argb(255, red, green, blue)
                        result.add(Keyword(Utils.calculateBreakWord(keyword), color))
                    }
                    hideLoading()
                    view.onGetKeywordsCompleted(result)
                }, {
                    hideLoading()
                    view.onGetKeywordsCompleted(ArrayList(), it)
                    Log.e("HomePresenter", "getKeyword() -> $it")
                })
        )
    }
}