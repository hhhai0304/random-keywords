package com.hohoanghai.tikihometest

import com.hohoanghai.tikihometest.common.util.Utils
import org.junit.Assert
import org.junit.Test

class TikiHomeTest {

    @Test
    fun test1Word() {
        val output = Utils.calculateBreakWord("a")
        Assert.assertEquals("a", output)
    }

    @Test
    fun test2Word() {
        val output = Utils.calculateBreakWord("Tiki Út")
        Assert.assertEquals("Tiki\nÚt", output)
    }

    @Test
    fun test3Word() {
        val output = Utils.calculateBreakWord("Tiki Ở Út Tịch")
        Assert.assertEquals("Tiki Ở\nÚt Tịch", output)
    }

    @Test
    fun test4Word() {
        val output = Utils.calculateBreakWord("Tiki a b Nghiêng")
        Assert.assertEquals("Tiki a b\nNghiêng", output)
    }

    @Test
    fun testNWord() {
        val output = Utils.calculateBreakWord("Nguyễn Nhật Ánh Ngày xưa có một chuyện tình")
        Assert.assertEquals("Nguyễn Nhật Ánh Ngày\nxưa có một chuyện tình", output)
    }
}